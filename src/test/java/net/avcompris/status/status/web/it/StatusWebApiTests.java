package net.avcompris.status.status.web.it;

import java.util.stream.Stream;

import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.provider.Arguments;

import net.avcompris.commons3.api.tests.ApiTestsUtils;
import net.avcompris.commons3.api.tests.TestSpecParametersExtension;
import net.avcompris.commons3.api.tests.TestsSpec.TestSpec;
import net.avcompris.commons3.web.HealthCheck;
import net.avcompris.commons3.web.it.utils.AbstractWebApiTest;
import net.avcompris.commons3.web.it.utils.RestAssured;
import net.avcompris.status.api.ServiceStatus;
import net.avcompris.status.api.ServiceStatusHistory;
import net.avcompris.status.api.ServicesStatus;
import net.avcompris.status.api.ServicesStatusHistory;
import net.avcompris.status.api.StatusConfig;

@ExtendWith(TestSpecParametersExtension.class)
@RestAssured("${status.baseURL}")
public class StatusWebApiTests extends AbstractWebApiTest {

	public StatusWebApiTests(final TestSpec spec) {

		super(spec, "toto");
	}

	public static Stream<Arguments> testSpecs() throws Exception {

		return ApiTestsUtils.testSpecs( //
				HealthCheck.class, //
				StatusConfig.class, //
				ServiceStatus.class, //
				ServicesStatus.class, //
				ServiceStatusHistory.class, //
				ServicesStatusHistory.class);
	}
}
